package main

import (
	"encoding/base64"
	"fmt"
	"github.com/spacemonkeygo/openssl"
	"io/ioutil"
	"log"
	"os"
)

/* structure to decode text */
type Crypter struct {
	key    []byte
	iv     []byte
	cipher *openssl.Cipher
}

const (
	filenameRes = "results.txt"
)

/* function to handle error */
func handleError(err error) {

	/* handle error */
	if err != nil {
		log.Fatal(err)
	}

}

/* function to create new decoder */
func NewCrypter(key []byte, iv []byte) (*Crypter, error) {

	var (
		err    error
		cipher *openssl.Cipher
	)

	/* create new decoder */
	cipher, err = openssl.GetCipherByName("aes-128-ctr")
	if err != nil {
		return nil, err
	}

	/* return decoder */
	return &Crypter{key, iv, cipher}, nil

}

/* function to encode text */
func (c *Crypter) Encrypt(input []byte) ([]byte, error) {

	var (
		err           error
		ctx           openssl.EncryptionCipherCtx
		cipherbytes   []byte
		finalbytes    []byte
	)

	/* create context */
	ctx, err = openssl.NewEncryptionCipherCtx(c.cipher, nil, c.key, c.iv)
	if err != nil {
		return nil, err
	}

	/* do the first stage of encoding */
	cipherbytes, err = ctx.EncryptUpdate(input)
	if err != nil {
		return nil, err
	}

	/* do the second stage of encoding */
	finalbytes, err = ctx.EncryptFinal()
	if err != nil {
		return nil, err
	}

	/* combine intermediate results and send answer */
	cipherbytes = append(cipherbytes, finalbytes...)
	return cipherbytes, nil

}

/* encode byte array using base64 format */
func base64Encode(message []byte) []byte {

	var (
		byteArray []byte
	)

	/* encode byte array */
	byteArray = make([]byte, base64.StdEncoding.EncodedLen(len(message)))
	base64.StdEncoding.Encode(byteArray, message)
	return byteArray

}

/* function to write message and read row */
func readRow(mes string, row *string) {

	var (
		err error
	)

	/* function to read row and write message */
	fmt.Println(mes)
	_, err = fmt.Scanln(row)
	handleError(err)

}

/* function to read file and return full row */
func readFile(filename string) []byte {

	var (
		err    error
		line   []byte
	)

	/* open and read file */
	line, err = ioutil.ReadFile(filename)
	handleError(err)

	/* return result */
	return line

}

/* function to write results */
func writeResults(res []byte) {

	var (
		file *os.File
		err  error
	)

	/* create file */
	file, err = os.Create(filenameRes)
	handleError(err)

	/* write file */
	_, err = file.Write(res)
	handleError(err)

}

func main() {

	var (
		key, iv         string
		crypter         *Crypter
		filename        string
		err             error
		encArray        []byte
		encBase64Array  []byte
		initArray       []byte
	)

	/* read parameters */
	readRow("Key for encryption:", &key)
	readRow("Initial vector for encryption:", &iv)
	readRow("Filename for encryption:", &filename)
	/* read row from file */
	initArray = readFile(filename)

	/* create decoder */
	crypter, err = NewCrypter([]byte(key), []byte(iv))
	handleError(err)

	/* encode text */
	encArray, err = crypter.Encrypt(initArray)

	/* encode cypher text using base64 format */
	encBase64Array = base64Encode(encArray)

	/* write results */
	writeResults(encBase64Array)

}